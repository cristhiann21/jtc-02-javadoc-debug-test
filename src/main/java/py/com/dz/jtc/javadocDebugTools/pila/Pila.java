package py.com.dz.jtc.javadocDebugTools.pila;

public class Pila<TipoDeDato> implements InterfazPila<TipoDeDato> {

    private NodoPila<TipoDeDato> cabecera;
    
    private int tamanoPila;
    
    public Pila() {
        tamanoPila = 0;
        cabecera = new NodoPila<TipoDeDato>();
        cabecera.setSiguiente(null);
    }
        
    public void apilar(TipoDeDato dato){
    	NodoPila<TipoDeDato> nuevoNodo = new NodoPila<TipoDeDato>();
        nuevoNodo.setDato(dato);
        nuevoNodo.setSiguiente(cabecera.getSiguiente());
        cabecera.setSiguiente(nuevoNodo);
        tamanoPila = tamanoPila + 1;
    }
    
    public TipoDeDato desapilar(){
    	TipoDeDato ret = null;
    	if (!esVacia()) {
    	    ret = cabecera.getSiguiente().getDato();
            cabecera.setSiguiente(cabecera.getSiguiente().getSiguiente());
            tamanoPila = tamanoPila - 1;
        }
        return ret;
    }
    
    public TipoDeDato obtenerTope(){
    	TipoDeDato ret = null;
    	if (!esVacia()) {
    	    ret = cabecera.getSiguiente().getDato();            
        }
        return ret;
    }
    
    public void anular(){
    	cabecera.setSiguiente(null);
    	tamanoPila = 0;
    }
    
    public int tamanoPila() {
        return tamanoPila;
    }

    public boolean esVacia() {
        if (tamanoPila == 0) {
            return true;
        } else {
            return false;
        }
    }  
    
} //Fin de clase Pila
