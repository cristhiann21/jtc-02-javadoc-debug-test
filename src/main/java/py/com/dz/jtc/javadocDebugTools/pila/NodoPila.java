package py.com.dz.jtc.javadocDebugTools.pila;

/**
 * Clase que representa a un NodoPila
 * <br>Curso de Programacion Java
 * @author Derlis Zarate
 */
public class NodoPila<TipoDeDato> {

    /**
     * Dato a almacenar en el nodo de la pila
     */
    private TipoDeDato dato;
    
    /**
     * Puntero al siguiente nodo
     */
    private NodoPila<TipoDeDato> siguiente;

    /**
     * GetDato retorna el dato del nodo
     * @return El dato almacenado en el nodo
     */
    public TipoDeDato getDato() {
        return dato;
    }

    /**
     * SetDato setea el dato del nodo
     * @param dato the dato to set
     */
    public void setDato(TipoDeDato dato) {
        this.dato = dato;
    }

    /**
     * GetSiguiente
     * @return the siguiente
     */
    public NodoPila<TipoDeDato> getSiguiente() {
        return siguiente;
    }

    /**
     * SetSiguiente
     * @param siguiente the siguiente to set
     */
    public void setSiguiente(NodoPila<TipoDeDato> siguiente) {
        this.siguiente = siguiente;
    }
    
} //Fin de clase NodoPila
