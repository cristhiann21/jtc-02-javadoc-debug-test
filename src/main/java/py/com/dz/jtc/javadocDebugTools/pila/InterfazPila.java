package py.com.dz.jtc.javadocDebugTools.pila;

/**
 * Clase que representa a una InterfazPila
 * <br>Curso de Programacion Java
 * @author Derlis Zarate
 */
public interface InterfazPila<TipoDeDato> {
        
	/**
	 * Inserta el dato dentro de la pila en el tope de la misma
	 * @param dato Dato a insertar en la pila
	 */
    public void apilar(TipoDeDato dato);
    
    /**
     * Elimina el dato ubicado en el tope de la pila y lo retorna como resultado
     * @return el dato almacenado en el tope de la pila
     */
    public TipoDeDato desapilar();
    
    /**
     * Accede y retorna el dato ubicado en el tope de la pila, sin eliminarlo
     * @return el dato almacenado en el tope de la pila
     */
    public TipoDeDato obtenerTope();
    
    /**
     * Metodo que anula y elimina todos los elementos de la pila, dejandola vacia
     */
    public void anular();
    
    /**
     * Metodo que retorna la cantidad de elementos actuales de la pila
     * @return el nro. de elementos de la pila
     */
    public int tamanoPila();
        
    /**
     * Metodo que retorna TRUE o FALSE segun la pila este o no vacia
     * @return TRUE o FALSE si la pila esta vacia o no.
     */
    public boolean esVacia();
    
} //Fin de clase InterfazPila
